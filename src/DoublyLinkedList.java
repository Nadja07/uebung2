

public class DoublyLinkedList {

    protected Element head;
    protected Element end;
    protected int size; //number of elements in the list; is increased if an element is appended, prepended, or inserted; and decreased if an element is removed


    public DoublyLinkedList() {
        this.head = null;
        this.end = null;
        this.size = 0;
    }


    // a) prepend - Element am Anfang einfügen
    public void prepend(Element e) {

        if (this.isEmpty()) {
            this.end = this.head = e;
            this.head.successor = null;
            this.end.predecessor = null;

        } else {
            this.head.predecessor = e;
            e.successor = this.head;
            this.head = e;
            this.head.predecessor = null;

        }

        this.size++;
    }


    // b) append - Element am Ende einfügen
    public void append(Element e) {

        if (this.isEmpty()) {

            this.end = this.head = e;
            this.head.predecessor = null;
            this.end.successor = null;

        } else {

            this.end.successor = e;
            e.predecessor = this.end;
            this.end = e;
            this.end.successor = null;

        }
        this.size++;
    }

    // c) insert - Element an einer angegebenen Position einzufügen
    public void insert(int index, Element e) {

        if (this.isEmpty()) {
            this.append(e);
        }

        if (index == 0) {
            this.prepend(e);
        } else {

            Element temp1 = this.get(index - 1);
            Element temp2 = this.get(index);

            temp1.successor = e;
            e.successor = temp2;
            this.size++;
        }
    }


    // d) get - ein Element an einer gegebenen Position abrufen
    public Element get(int index) throws NullPointerException {

        if (this.isEmpty()) {
            throw new NullPointerException();
        } else if (index < 0 || index > this.getSize() - 1) {
            throw new IndexOutOfBoundsException();
        } else {
            int center = this.getSize() / 2;

            // -> linke haelfte der liste durchsuchen mit head & successor

            if (index <= center) {

                Element e = this.head;

                for (int i = 0; i < index; i++) {

                    e = e.successor;
                }

                return e;
            }

            // index > center -> rechte haelfte der liste durchsuchen mit end & predecessor
            else {

                Element e = this.end;

                for (int i = this.getSize() - 1; i > index; i--) {

                    e = e.predecessor;

                }

                return e;

            }

        }
    }


    // e) remove - Element an einer angegebenen Position löschen
    public void remove(int index) throws NullPointerException {

        if (this.isEmpty()) {
            throw new NullPointerException();
        } else {
            Element e = this.get(index);
            Element temp1 = this.get(index - 1);
            Element temp2 = this.get(index);

            temp1.successor = e.getSuccessor();
            temp2.predecessor = e.getPredecessor();
        }

        this.size--;

        if (this.size == 0) {
            this.head = null;
            this.head.successor = null;
        }
    }

    // f) contains - prüft, ob das übergebene Element in der Liste enthalten ist
    public boolean contains(Element e) {

        if (this.isEmpty()) {
            return false;
        } else {

            for (int i = 0; i < this.getSize(); i++) {

                Element current = this.get(i);
                if (current.equals(e)) {
                    return true;
                }
            }

            return false;
        }
    }

    //returns true if list is empty, false else
    public boolean isEmpty() {
        return this.head == null;
    }

    //returns the number of elements in the list
    public int getSize() throws NullPointerException {

        if (this.isEmpty()) {

            throw new NullPointerException();
        } else {
            return this.size;
        }
    }


    //prints all elements of the list from start to end
    public void print() {
        if (this.isEmpty()) {

            System.out.println("Empty list!");

        } else {


            for (int i = 0; i < this.getSize(); i++) {

                System.out.println(this.get(i).getValue());
            }
        }
    }

    //prints all elements in the list in reverse order (from end to start)
    public void printReverse() throws NullPointerException {

        if (this.isEmpty()) {

            System.out.println("Empty list!");

        } else {

            int i;

            for (i = this.getSize() - 1; i >= 0; i--) {

                System.out.println(this.get(i).getValue());
            }
        }
    }
}











import de.htwberlin.fiw.profiler.ProfiledClass;
import de.htwberlin.fiw.profiler.Profiler;

import java.util.Random;


public class Main extends ProfiledClass {

    public void run() {

        Set mySet = new Set();
        DoublyLinkedList doublyLinkedList = new DoublyLinkedList();

        for (int i = 0; i < 10000; i++) {
            Element element = new Element(new Random().nextInt(10000));
            doublyLinkedList.prepend(element);

        }
        doublyLinkedList.print();
        doublyLinkedList.printReverse();

        for (int i = 0; i < 10000; i++) {
            Element element = new Element(new Random().nextInt(10000));
            mySet.add(element);

        }
        mySet.print();
        mySet.printReverse();


    }

    public static void main(String[] args) {

        Profiler profiler = new Profiler(Main.class);
        profiler.start();
        profiler.printResults();

    }

}


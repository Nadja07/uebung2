
public class Set extends DoublyLinkedList {


    public Set() {

        super();
    }

    public void add(Element e) {

        if (this.isEmpty()) {
            this.head = e;
            this.end = this.head;
            this.head.predecessor = null;
            this.size++;

        } else {


            if (!this.contains(e)) {

                this.prepend(e);
            }


        }

    }


    public int findInsertPosition(Element e) {

        int i;

        for (i = 0; i < this.getSize() - 1; i++) {

            if (e.getValue() > this.get(i).getValue()) {
                return i;
            }
        }
        return i;
    }
}
